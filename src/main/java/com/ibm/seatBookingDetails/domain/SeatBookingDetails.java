package com.ibm.seatBookingDetails.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Entity class for User details
 * @author LingarajSahoo
 *
 */
@Entity
@Table(name="SEATBOOKINGDETAILS")
public class SeatBookingDetails {
	 @Id @GeneratedValue
	 @Column(name = "seat_id")
	 private int seatId;
	 @Column(name = "floor_number")
	 private int floorNumber;
	 @Column(name = "office_id")
	 private String officeId;
	 @Column(name = "city_name")
	 private String cityName;
	 @Column(name = "seat_name")
	 private String seatName;
	 @Column(name = "status")
	 private String status;
	 @Column(name = "timings")
	 private String timings;

	 public SeatBookingDetails() {
	    }

	    public SeatBookingDetails(int seatId, int floorNumber,String officeId,  String cityName,String seatName, String status,String timings) {
	        this.seatId = seatId;
	        this.floorNumber = floorNumber;
	        this.officeId = officeId;
	        this.cityName = cityName;
	        this.seatName = seatName;
	        this.status = status;
	        this.timings = timings;
	    }
	    public int getSeatId() {
			return seatId;
		}

		public void setSeatId(int seatId) {
			this.seatId = seatId;
		}

		public int getFloorNumber() {
			return floorNumber;
		}

		public void setFloorNumber(int floorNumber) {
			this.floorNumber = floorNumber;
		}

		public String getOfficeId() {
			return officeId;
		}

		public void setOfficeId(String officeId) {
			this.officeId = officeId;
		}

		public String getCityName() {
			return cityName;
		}

		public void setCityName(String cityName) {
			this.cityName = cityName;
		}

		public String getSeatName() {
			return seatName;
		}

		public void setSeatName(String seatName) {
			this.seatName = seatName;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getTimings() {
			return timings;
		}

		public void setTimings(String timings) {
			this.timings = timings;
		}
	    
	    @Override
	    public String toString() {
	        return "SeatBooking{" +
	                "seatId=" + seatId +
	                ", floorNumber='" + floorNumber + '\'' +
	                ", officeId='" + officeId + '\'' +
	                ", cityName='" +cityName + '\'' +
	                ", seatName='" +seatName + '\'' +
	                ", status='" + status + '\'' +
	                ", timings='" + timings + '\'' +
	                '}';
	    }
}
