package com.ibm.seatBookingDetails.service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ibm.seatBookingDetails.domain.SeatBookingDetails;
import com.ibm.seatBookingDetails.repository.SeatBookingDetailsRepository;
/**
 * User service implementation class.
 * 
 * @author LingarajSahoo
 *
 */
@Service
public class SeatBookingDetailsServiceImpl implements SeatBookingDetailsService{
	@Autowired
	 private SeatBookingDetailsRepository seatBookingDetailsRepository;
	    /**
	     * Saving seat booking details
	     */
	  
	  public SeatBookingDetails saveSeatBookingDetails(SeatBookingDetails seatBookingDetails)  {
	    	seatBookingDetailsRepository.save(seatBookingDetails);
	        return seatBookingDetails;
	    }
	    
	    /*
		 * This method should be used to update a existing category.Call the
		 * corresponding method of Respository interface.
		 */
		public SeatBookingDetails updateSeatBookingDetails(SeatBookingDetails seatBookingDetails, int seatId) {

			Optional<SeatBookingDetails> seatBookingDetailsforUpdate = this.seatBookingDetailsRepository.findById(seatId);
			if(seatBookingDetailsforUpdate.isPresent()) {
				seatBookingDetails.setSeatId(seatId);
				this.seatBookingDetailsRepository.save(seatBookingDetails);
				return seatBookingDetailsforUpdate.get();
			}
			else {
				return null;
			}
		}
		
		public SeatBookingDetails bookSeats(int seatId) {

			Optional<SeatBookingDetails> seatBookingDetailsforUpdate = this.seatBookingDetailsRepository.findById(seatId);
			if(seatBookingDetailsforUpdate.isPresent()) {
				seatBookingDetailsforUpdate.get().setStatus("Booked");
				this.seatBookingDetailsRepository.save(seatBookingDetailsforUpdate.get());
				return seatBookingDetailsforUpdate.get();
			}
			else {
				return null;
			}
		}
		
		public SeatBookingDetails unbookSeats(int seatId) {

			Optional<SeatBookingDetails> seatBookingDetailsforUpdate = this.seatBookingDetailsRepository.findById(seatId);
			if(seatBookingDetailsforUpdate.isPresent()) {
				seatBookingDetailsforUpdate.get().setStatus("Available");
				this.seatBookingDetailsRepository.save(seatBookingDetailsforUpdate.get());
				return seatBookingDetailsforUpdate.get();
			}
			else {
				return null;
			}
		}


		/*
		 * This method should be used to get a category by userId.Call the corresponding
		 * method of Respository interface.
		 */
		public List<SeatBookingDetails> getAllAvailableBookingDetailsByCityNameOfficeId(String cityName,String officeId) {

			List<SeatBookingDetails> findAllBookingDetailsByCityNameOfficeId = this.seatBookingDetailsRepository.
					findByCityNameAndOfficeIdAndStatus(cityName,officeId,"Available",Sort.by(Sort.Direction.ASC, "seatId"));
			if(findAllBookingDetailsByCityNameOfficeId != null) {
				return findAllBookingDetailsByCityNameOfficeId;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
		public List<SeatBookingDetails> getAllBookedSeatDetailsByCityNameOfficeId(String cityName,String officeId) {

			List<SeatBookingDetails> findAllBookingDetailsByCityNameOfficeId = this.seatBookingDetailsRepository.
					findByCityNameAndOfficeIdAndStatus(cityName,officeId,"Booked",Sort.by(Sort.Direction.ASC, "seatId"));
			if(findAllBookingDetailsByCityNameOfficeId != null) {
				return findAllBookingDetailsByCityNameOfficeId;
			}
			else {
				throw new NoSuchElementException();
			}
		}
}
