package com.ibm.seatBookingDetails.repository;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ibm.seatBookingDetails.domain.SeatBookingDetails;

@Repository
public interface SeatBookingDetailsRepository extends JpaRepository<SeatBookingDetails,Integer>{
	
	/*
	 * This method will search for all seat details in data source which are created
	 * by specific to city name and office id
	 *
	 */
	List<SeatBookingDetails> findByCityNameAndOfficeIdAndStatus(String cityName,String officeId,String status,Sort sort);
	/*
	 * @Query(value =
	 * "SELECT sb.seat_id,sb.city_name,sb.floor_number,sb.office_id,sb.seat_name,sb.status,sb.timings "
	 * + "FROM seatbookingdetails sb  "+
	 * "inner join officedetails of on sb.office_id = of.office_id where sb.city_name = :cityName "
	 * + "AND of.office_name=:officeId",nativeQuery = true) List<SeatBookingDetails>
	 * findByCityNameAndOfficeId(String cityName,String officeId);
	 */

}
