package com.ibm.seatBookingDetails.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.ibm.seatBookingDetails.domain.SeatBookingDetails;
import com.ibm.seatBookingDetails.exception.BookingDetailsNotFoundException;
import com.ibm.seatBookingDetails.service.SeatBookingDetailsService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Controller layer test cases for Seat booking details.
 * 
 * @author LingarajSahoo
 *
 */
@RestController
@RequestMapping("/api/v1/seatBookingDetails")
public class SeatBookingDetailsController {
	@Autowired
	private SeatBookingDetailsService seatBookingDetailsService;
	private ResponseEntity<?> responseEntity;

	
	/**
	 * Registering seat booking details controller
	 * 
	 * @param OfficeDetails
	 * @return
	 */
	@RequestMapping(value ="/insert",method = RequestMethod.POST)
	@CrossOrigin(origins = "*")
	public ResponseEntity<?> registerSeat(@RequestBody SeatBookingDetails seatBookingDetails)  {
		try {
			SeatBookingDetails seatBookingDetailsObj = this.seatBookingDetailsService.saveSeatBookingDetails(seatBookingDetails);
			return responseEntity = new ResponseEntity(seatBookingDetailsObj, HttpStatus.CREATED);
		} catch (Exception exp) {
			responseEntity = new ResponseEntity("Try after sometime", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	/**
	 * update seat booking details controller
	 * 
	 * @param seatBookingDetails
	 * @param seatId
	 * @return
	 */

	@RequestMapping(value = "/{id}",method = RequestMethod.PUT)
	@CrossOrigin(origins = "*")
	public ResponseEntity<String> updateSeatBookingDetailsFromDB(@RequestBody SeatBookingDetails seatBookingDetails, @PathVariable("id") int seatId) {
		
		SeatBookingDetails updateSuccess = this.seatBookingDetailsService.updateSeatBookingDetails(seatBookingDetails, seatId);
			if (updateSuccess != null) {
				return new ResponseEntity(HttpStatus.OK);
			}
			else {
				return new ResponseEntity(HttpStatus.CONFLICT);
			}
		
	}
	
	@RequestMapping(value = "/bookSeats/{id}",method = RequestMethod.GET)
	@CrossOrigin(origins = "*")
	public ResponseEntity<String> bookSeats(@PathVariable("id") int seatId) {
		
		SeatBookingDetails updateSuccess = this.seatBookingDetailsService.bookSeats(seatId);
			if (updateSuccess != null) {
				return new ResponseEntity(updateSuccess,HttpStatus.OK);
			}
			else {
				return new ResponseEntity(HttpStatus.CONFLICT);
			}
		
	}
	
	@RequestMapping(value = "/unbookSeats/{id}",method = RequestMethod.GET)
	@CrossOrigin(origins = "*")
	public ResponseEntity<String> unBookSeats(@PathVariable("id") int seatId) {
		
		SeatBookingDetails updateSuccess = this.seatBookingDetailsService.unbookSeats(seatId);
			if (updateSuccess != null) {
				return new ResponseEntity(updateSuccess,HttpStatus.OK);			}
			else {
				return new ResponseEntity(HttpStatus.CONFLICT);
			}
		
	}
	
	/*
	 * Define a handler method which will get us the Booking details by a city name and book.
	 * 
	 * This handler method should return any one of the status messages basis on
	  */
	
	@RequestMapping(value ="/{cityName}/{officeId}",method = RequestMethod.GET)
	@CrossOrigin(origins = "*")
	public ResponseEntity<String> getAllAvailableBookingDetailsByCityNameOfficeId(@PathVariable("cityName") String cityName,@PathVariable("officeId") String officeId) throws BookingDetailsNotFoundException {
		
		try {
		
			List<SeatBookingDetails> seatBookingDetailsList = this.seatBookingDetailsService.getAllAvailableBookingDetailsByCityNameOfficeId(cityName,officeId);
			if(seatBookingDetailsList != null) {
				return new ResponseEntity(seatBookingDetailsList,HttpStatus.OK);
			}
			else {
				return new ResponseEntity(seatBookingDetailsList,HttpStatus.NOT_FOUND);
			}
	   } 
		
		catch(Exception e) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}

  }
	
	@RequestMapping(value ="/getBookedSeats/{cityName}/{officeId}",method = RequestMethod.GET)
	@CrossOrigin(origins = "*")
	public ResponseEntity<String> getAllBookedSeatDetailsByCityNameOfficeId(@PathVariable("cityName") String cityName,@PathVariable("officeId") String officeId) throws BookingDetailsNotFoundException {
		
		try {
		
			List<SeatBookingDetails> seatBookingDetailsList = this.seatBookingDetailsService.getAllBookedSeatDetailsByCityNameOfficeId(cityName,officeId);
			if(seatBookingDetailsList != null) {
				return new ResponseEntity(seatBookingDetailsList,HttpStatus.OK);
			}
			else {
				return new ResponseEntity(seatBookingDetailsList,HttpStatus.NOT_FOUND);
			}
	   } 
		
		catch(Exception e) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}

  }
	
}
